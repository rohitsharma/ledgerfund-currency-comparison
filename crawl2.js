/**
 * Created by darwin on 21/7/17.
 */
var fs = require('fs');
var async = require('async');
var Promise = require("promise");
var request = require('request');
var cheerio = require('cheerio');
var Currency = require('./schema/currencyDetail');

/*var dbURI = 'mongodb://localhost/currencyinfo';
if(process.env.NODE_ENV === 'prod'){
    dbURI = 'mongodb://localhost/currencyinfo';
}
mongoose.connect(dbURI, {useMongoClient: true});*/
var coinMarketcapObj = JSON.parse(fs.readFileSync('currency.json', 'utf8'));
function CryptoCompare() {
}
CryptoCompare.prototype.getCryptoCompare = function () {
    console.log("in cryto compare");
    return new Promise(function(resolve, reject) {
        getCoinList()
            .then(processArray.bind(null, getSocialStats))
            .then(function (msg) {
                console.log("doneeeeeeeeeeeeee");
                resolve();
            })
            .catch(function (err) {
                console.log(err);
                reject(err);
            });
    });
};

function getCoinList() {
    return new Promise(function(resolve, reject) {
        var idArr =[];
        request({
            headers: {
                'Accept': 'text/html,application/xhtml+xml,application/xml,application/json, text/javascript',
                'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0',
            },
            uri: 'https://www.cryptocompare.com/api/data/coinlist/',
            method: 'GET'
        }, function (err, response, html) {
            if (err) {
                reject(err);
            } else {
                var parsedJSON = JSON.parse(html);
                var json = parsedJSON.Data;
                for(var index in coinMarketcapObj){
                    var coinMarketSymbol = coinMarketcapObj[index].symbol;
                    for(var key in json){
                        if(key === coinMarketSymbol){
                            idArr.push(json[key].Id);
                        }
                    }
                }
                resolve(idArr);
            }
        });
    });
}
function processArray(fn, uniqueID) {
    //var coinGeckoData = [];
    return uniqueID.reduce(function (p, item) {
        return p.then(function () {
            return fn(item).then(function () {
               return "All currencies done";
            });
        });
    }, Promise.resolve());
}

function getSocialStats(id) {
    return new Promise(function(resolve, reject) {
        var githubData = {};
        var finalDataObj = {};
        request({
            headers: {
                'Accept': 'text/html,application/xhtml+xml,application/xml,application/json, text/javascript',
                'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0',
            },
            uri: 'https://www.cryptocompare.com/api/data/socialstats/?id='+id,
            method: 'GET'
        }, function (err, response, html) {
            if (err) {
                reject(err);
            } else if(response && response.statusCode === 200) {
                var parsedJSON = JSON.parse(html);
                var data = parsedJSON.Data;
                var symbol = data.General.Name || "";
                var name = data.General.CoinName || "";
                if(name){
                    name = name.toLowerCase().trim();
                    var boolean = hasWhiteSpace(name);
                    if(boolean){
                        name=name.replace(/ /g,"-");
                    }
                }
                var facebookLikes = data.Facebook.likes || 0;
                var twitterFollowers = data.Twitter.followers || 0;
                var redditSubscribers = data.Reddit.subscribers || 0;
                var githubDataObject = data.CodeRepository;
                var githubArr = githubDataObject.List;
                if (Object.prototype.toString.call(githubArr) === '[object Array]'
                    && githubArr.length > 0){
                    finalDataObj = {
                        name : name,
                        symbol:symbol,
                        fbLikes : facebookLikes,
                        twitterFollowers : twitterFollowers,
                        redditSubscribers : redditSubscribers,
                        forks : githubArr[githubArr.length-1].forks,
                        watchers: githubArr[githubArr.length-1].subscribers,
                        stars : githubArr[githubArr.length-1].stars
                    };
                } else{
                    finalDataObj = {
                        fbLikes : 0,
                        symbol:"",
                        twitterFollowers : 0,
                        redditSubscribers : 0,
                        name : name,
                        forks : 0,
                        watchers: 0,
                        stars : 0
                    };
                }
                console.log(finalDataObj.symbol);
                Currency.update({"symbol":finalDataObj.symbol}, { $set:{
                        fbLikes: finalDataObj.fbLikes,
                        twitterFollowers: finalDataObj.twitterFollowers,
                        redditSubscribers: finalDataObj.redditSubscribers,
                        forks:finalDataObj.forks,
                        watchers:finalDataObj.watchers,
                        stars:finalDataObj.stars
                    }
                }, function(err,result) {
                    console.log(result);
                    if (err) {
                        console.log(err);
                        // handle error
                    } else if(!result){
                        console.log("Not inserted");
                    }else{
                        console.log(result);
                    }
                });
                resolve();
            }
        });
    });
}

function hasWhiteSpace(s) {
    return s.indexOf(' ') >= 0;
}

module.exports = new CryptoCompare();


