var dataService = require('../services/dataService.js');
module.exports = {
	createRouter: function(app) {
		app.post('/getData', function(req, res){
			dataService.getData(req, res);
		});
		app.post('/getExchangeList', function(req, res)	{
			dataService.getExchangeData(req, res);
		});
	}
}