 /**
 * Created by darwin on 17/5/17.
 */
"use strict";
var express =require('express');
var app  = express();
var path = require('path');
var bodyParser  = require("body-parser");
var mongoose = require('mongoose');
const router = require('./routes/router');
var dbURI = 'mongodb://localhost/currencyinfo';
if(process.env.NODE_ENV === 'prod'){
    dbURI = 'mongodb://localhost/currencyinfo'
}
mongoose.connect(dbURI);
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers','Content-Type,Authorization');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    // res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
router.createRouter(app);

var port = process.env.PORT || 4002;
const server = app.listen(port, function() {
  console.log(`Server listening on port ${server.address().port}`);
});
