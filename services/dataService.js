var request =require('request');
var Currency = require('../schema/currencyDetail.js');
var async = require('async');
module.exports = {
	getData: function(req, res) {
		var requestOptions = {
        headers: {
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0',
        },
        uri: 'https://api.coinmarketcap.com/v1/ticker/?convert=BTC',
        method: 'GET'
		};
		request(requestOptions, function (error, html, response) {
            var arr = [];
            if(error){
				console.log("Did nt get valid response from coin market api");
			} else {
				var apiResponse = JSON.parse(response);
				for(var i = 0; i<apiResponse.length; i++){
					for(var j =0;j<req.body.currencies.length;j++){
					 	if(apiResponse[i].id === req.body.currencies[j]){
					 		var obj = {};
					 		obj.currency = apiResponse[i].name;
					 		obj.id = apiResponse[i].id;
					 		obj.coinMCapLink = 'https://coinmarketcap.com/currencies/'+apiResponse[i].id+'/';
						 	for(var k= 0; k < req.body.params.length; k++){
						 		if(req.body.params[k] === "24h_volume_btc"){
				 					obj["Volume24hrBTC"] = parseInt(apiResponse[i][req.body.params[k]]);
						 		} else if(req.body.params[k] === "24h_volume_usd"){
				 					obj["Volume24hrUSD"] = parseInt(apiResponse[i][req.body.params[k]]).toLocaleString('en-US');
						 		}else if(req.body.params[k] === "price_btc"){
				 					obj["BTCPrice"] = apiResponse[i][req.body.params[k]];
						 		}else if(req.body.params[k] === "price_usd"){
				 					obj["USDPrice"] = parseInt(apiResponse[i][req.body.params[k]]).toLocaleString('en-US');
						 		}else if(req.body.params[k] === "market_cap_btc"){
				 					obj["MarketCapBTC"] = apiResponse[i][req.body.params[k]];
						 		}else if(req.body.params[k] === "total_supply"){
				 					obj["TotalSupply"] = apiResponse[i][req.body.params[k]];
						 		}else if(req.body.params[k] === "available_supply"){
				 					obj["CirculatingSupply"] = apiResponse[i][req.body.params[k]];
						 		}else if(req.body.params[k] === "market_cap_usd"){
                                    obj["MarketCapUSD"] = parseInt(apiResponse[i][req.body.params[k]]).toLocaleString('en-US');
                                }else if(req.body.params[k] === "vol_market_btc_ratio"){
                                    obj["BTCRatio"] = apiResponse[i]["24h_volume_btc"]/apiResponse[i]["market_cap_btc"];
                                }else if(req.body.params[k] === "vol_market_usd_ratio"){
                                    obj["USDRatio"] = parseInt(apiResponse[i]["24h_volume_usd"]/apiResponse[i]["market_cap_usd"]).toLocaleString('en-US');
                                }else{
						 			obj[req.body.params[k]] = apiResponse[i][req.body.params[k]];
						 		}
					 		}
				 			arr.push(obj);
				 		}
					}
				}
			}
			getCurrencyDetails(arr,req, res);
		});
	},

	getExchangeData: function(req, res){
		var query;
		if(req.body.data === "bitcoin"){
			query = {"slug":"bitcoin"};
		}else{
            query = {"slug":{$in:["bitcoin", req.body.data]}};
        }
		Currency.find(query, function(err, result){
			var listed = [];
			var notListed;
			if(err) return res.status(200).send({
                success: true,
                message: 'No exchange found'
    		});
			if(result && result.length <=1){
				listed = result[0].exchangeList;
			}else {
                listed = result[1].exchangeList;
            }
			notListed = result[0].exchangeList.filter(function(val) {
				return listed.indexOf(val) === -1;
			});
			return res.status(200).send({
				success: true,
				data:{
					"listed":listed,
					"notListed":notListed
				}
			});
		});

	}
};
function getCurrencyDetails(arr, req, res) {
	console.log(req.body.params);
	var finalResult = [];
	async.eachSeries(arr, function(item, callback){
		var id = item.id;
		Currency.findOne({slug:id}, function(err, dbResult){
			if(err) return err;
			else{
				dbResult = dbResult.toObject();
				item.link = dbResult.link;
				if(req.body.params.indexOf("noOfExchange") > -1){
					item.noOfExchange = dbResult.exchangeCount;
				}
				if(req.body.params.indexOf("exchangeList") > -1){	
					item.exchangeList = dbResult.exchangeList;
				}
                if(req.body.params.indexOf("socialStats") > -1){
					console.log("***************");
                    item.socialStats = {
                        "forks":dbResult.forks,
						"watchers":dbResult.watchers,
						"stars":dbResult.stars,
						"fbLikes":dbResult.fbLikes,
						"twitterFollowers":dbResult.twitterFollowers,
						"redditSubscribers":dbResult.redditSubscribers
                    };
                }
                if(req.body.params.indexOf("githubStats") > -1) {
                    item.githubStats = {
                        "forks":dbResult.forks,
                        "watchers":dbResult.watchers,
                        "stars":dbResult.stars
                    };
                }
				finalResult.push(item);
			}
			callback();
		});
	},function(err){
		if(err){
			return res.status(400).send({
                success: false,
                message: 'Didnot get valid response frm server',
                data: []
    		});	
		} else{
			return res.status(200).send({
                success: true,
                message: 'Data collected',
                data: finalResult
    		});	
		}
		
 	});
}