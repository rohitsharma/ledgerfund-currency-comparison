(function () {
  'use strict';
  mainController.$inject = ['$scope', '$http', 'usSpinnerService'];
  function mainController($scope, $http, usSpinnerService) {
    $scope.currencymodel = [];
    $scope.paramsmodel = [];
    var arr = [];

   $http({
        method: 'GET',
        url: './currencyConfig.json'
    }).then(function (success){
        for(var i = 0;i<success.data.length;i++){
          var obj = {};
          obj.id = success.data[i].rank;  
          obj.label = success.data[i].name + ' '+ success.data[i].symbol;
          obj.slug = success.data[i].slug;
          arr.push(obj);
        }
         $scope.currencydata = arr;
    })
    .catch(function(){
        console.log("not found the currencies");
    });
    $scope.currencysettings = {enableSearch: true, smartButtonMaxItems: 3, scrollable:true, externalIdProp : ''  };
    $http({
        method: 'GET',
        url: './parameters.json'
    }).then(function (success){
         $scope.paramsdata = success.data;
    })
    .catch(function(){
        console.log("not found the currencies");
    });
    $scope.paramssettings = { enableSearch: true, smartButtonMaxItems: 3, scrollable:true, externalIdProp : '' };
    
    $scope.getData = function(){
      var postObject = {};
      postObject.currencies = [];
      postObject.params = [];
      for(var i = 0; i<$scope.currencymodel.length;i++){
         postObject.currencies.push($scope.currencymodel[i].slug);
      }
      for(var i = 0; i<$scope.paramsmodel.length;i++){
        postObject.params.push($scope.paramsmodel[i].slug);
      }
      $http({
        method: 'POST',
        url: 'http://127.0.0.1:4002/getData',
        data:postObject
        }).then(function (response){
            if(response.data.success){
              console.log(response.data.data);
              $scope.stopSpin();
              $scope.rowCollection = response.data.data;
              $scope.currency = response.data.data;
            }else{

            }
        })
        .catch(function(){
            console.log("not found the currencies");
        });
    }

    $scope.startSpin = function() {
          usSpinnerService.spin('spinner-1');
      };
      $scope.stopSpin = function() {
          usSpinnerService.stop('spinner-1');
      };

      $scope.getExchangeList = function(currencyObj){
        console.log(currencyObj.id);
        $http({
          method: 'POST',
          url: 'http://127.0.0.1:4002/getExchangeList',
          data:{"data":currencyObj.id}
        }).then(function (success){
            var exchangeData = success.data.data;
            $scope.listed = exchangeData.listed;
            $scope.notListed = exchangeData.notListed;

            console.log(exchangeData.listed);
            $('#myModal').modal('toggle');
        })
        .catch(function(){
          console.log("not found the currencies");
        });
      }
  }

  var app = angular.module('myApp', ['angularjs-dropdown-multiselect', 'angularSpinner', 'smart-table', '720kb.tooltips']);
  app.controller('mainController', mainController);
}());

