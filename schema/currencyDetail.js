'use strict';
var mongoose = require('mongoose');
var currencyDetail = mongoose.Schema({
    link:{
        type:String
    },
    exchangeCount : {
        type:Number,
        required : true
    },
    symbol :{
      type:String
    },
    exchangeList: {
        type:[]
    },
    slug:{
        type:String,
        required : true
    },
    fbLikes:{
      type:Number
    },
    twitterFollowers:{
      type:Number
    },
    redditSubscribers:{
      type:Number
    },
    forks:{
      type:Number
    },
    watchers:{
      type:Number
    },
    stars:{
      type:Number
    }
});
module.exports = mongoose.model('currencyDetail', currencyDetail);