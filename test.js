var cheerio = require('cheerio');
const table = `
  <table>
    <tr>
      <td></td>
      <td class='with-link'><a href='www.foo.bar'></a></td>
    </tr>
  </table>

`

const row = `<td><a href="www.foo.bar"></a></td>`

class Scraper {
  htmlToDom(html) {
    return cheerio.load(html)
  }
  findHref(row) {
    // console.log(row('a').attr('href'));
    return false;
    // return row('a').attr('href')
  }
}

const scraper = new Scraper()
const cheerioRow = scraper.htmlToDom(row)
// console.log(scraper.findHref(cheerioRow))

const cheerioTable = scraper.htmlToDom(table)
cheerioTable('tr').each(function() {
   let td = cheerioTable(this).find('td.with-link').html();
   console.log(td);
  // scraper.findHref(td);

})