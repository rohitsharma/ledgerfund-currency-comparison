var request = require('request');
var cheerio = require('cheerio');
var Currency = require('./schema/currencyDetail');
var fs = require('fs');
var async = require('async');
var obj = JSON.parse(fs.readFileSync('currency.json', 'utf8'));
var arr = [];

function CoinMarketCap() {
}
CoinMarketCap.prototype.getCMarketCapData = function () {
    return new Promise(function(resolve, reject) {
        async.eachSeries(obj, function (item, callback) {
            var exchangeString = '';
            var exchangeArray = [];
            var uniqueArray = [];
            request({
                headers: {
                    'Accept': 'text/html,application/xhtml+xml,application/xml,application/json, text/javascript',
                    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0',
                },
                uri: 'https://coinmarketcap.com/currencies/' + item.slug + '/#markets',
                method: 'GET'
            }, function (err, response, html) {
                    if (err) {
                    console.log("Didnot get valid response from api");
                    callback();
                } else if(response && response.statusCode === 200){
                    var obj = {};
                    var $ = cheerio.load(html);
                    var link = $('.list-unstyled li a').attr('href');
                    if (link) {
                        obj.link = link;
                    } else {
                        obj.link = "";
                    }
                    obj.slug = item.slug;
                    obj.symbol = item.symbol;
                    $('#markets-table tr').each(function (index, value) {
                        var v = $('td:nth-child(2)', this).text();
                        if (!v) {
                            exchangeString = '';
                            exchangeArray = [];
                        } else {
                            exchangeArray.push(v);
                        }
                    });
                    if (exchangeArray) {
                        uniqueArray = exchangeArray.filter(onlyUnique); // returns ['a', 1, 2, '1']
                    } else {
                        // uniqueList = 'Not listed';
                        uniqueArray = [];
                    }
                    obj.exchangeCount = uniqueArray.length;
                    // uniqueList = uniqueList.substring(0, uniqueList.length - 1);
                    obj.exchangeList = uniqueArray;
                    console.log(obj.symbol);
                    Currency.update({"symbol":obj.symbol}, obj, {upsert: true}, function(err,result) {
                        if (err) {
                            console.log(err);
                            // handle error
                        } else if(!result){
                        }else{
                            console.log(result);
                        }
                        callback();
                    });
                } else{
                    callback();
                }

            });

        }, function (err) {
        	if(err) return(err);
            console.log("coin market cap done");
            resolve();
        });
    });
};


function onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
}


module.exports = new CoinMarketCap();